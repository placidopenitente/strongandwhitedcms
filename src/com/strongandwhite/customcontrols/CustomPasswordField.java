package com.strongandwhite.customcontrols;

import com.strongandwhite.rules.Rule;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class CustomPasswordField extends CustomControl {
    private PasswordField passwordField;
    private Label textLabel;
    private Button clearButton;

    public CustomPasswordField(HBox control, ArrayList<CustomControl> customControls) {
        textLabel = (Label) ((VBox)control.getChildren().get(0)).getChildren().get(0);
        setErrorLabel((Label) ((VBox)control.getChildren().get(0)).getChildren().get(2));
        passwordField = (PasswordField) ((StackPane)((VBox)control.getChildren().get(0)).getChildren().get(1)).getChildren().get(0);
        clearButton = (Button) ((StackPane)((VBox)control.getChildren().get(0)).getChildren().get(1)).getChildren().get(1);
        textLabel.visibleProperty().bind(passwordField.textProperty().isNotEmpty());
        clearButton.visibleProperty().bind(passwordField.textProperty().isNotEmpty().and(passwordField.focusedProperty()));
        clearButton.setOnAction(event -> passwordField.clear());
        passwordField.textProperty().addListener(event -> executeRule(getRule(), passwordField.getText()));
        contentProperty().bind(passwordField.textProperty());
        customControls.add(this);
    }

    public String getText() {
        return passwordField.getText();
    }

    public void setText(String text) {
        passwordField.setText(text);
    }

    public StringProperty textProperty() {
        return passwordField.textProperty();
    }

    public BooleanProperty getEditableProperty() {
        return passwordField.editableProperty();
    }

    @Override
    public void clear() {
        passwordField.clear();
    }
}
