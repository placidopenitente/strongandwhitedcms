package com.strongandwhite.customcontrols;

import com.strongandwhite.rules.Rule;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

public abstract class CustomControl {
    private Rule rule;
    private Label errorLabel;
    private HBox container;
    private SimpleStringProperty content;

    public CustomControl() {
        content = new SimpleStringProperty("");
    }

    public boolean executeRule(Rule rule, String text) {
        errorLabel.setText("");
        if(!rule.validate(text)) {
            errorLabel.setText(rule.getRuleViolation());
            return false;
        }
        return true;
    }

    public abstract void clear();

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public Label getErrorLabel() {
        return errorLabel;
    }

    public void setErrorLabel(Label errorLabel) {
        this.errorLabel = errorLabel;
        errorLabel.setText("");
        errorLabel.visibleProperty().bind(errorLabel.textProperty().isNotEmpty());
    }

    public HBox getContainer() {
        return container;
    }

    public void setContainer(HBox container) {
        this.container = container;
    }

    public String getContent() {
        return content.get();
    }

    public SimpleStringProperty contentProperty() {
        return content;
    }

    public void setContent(String content) {
        this.content.set(content);
    }
}
