package com.strongandwhite.customcontrols;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.Collection;

public class CustomComboBox<T> extends CustomControl {
    private ComboBox<T> comboBox;
    private Label textLabel;

    public CustomComboBox(HBox control, ArrayList<CustomControl> customControls) {
        textLabel = (Label) ((VBox)control.getChildren().get(0)).getChildren().get(0);
        setErrorLabel((Label) ((VBox)control.getChildren().get(0)).getChildren().get(2));
        comboBox = (ComboBox<T>) ((StackPane)((VBox)control.getChildren().get(0)).getChildren().get(1)).getChildren().get(0);
        textLabel.visibleProperty().bind(comboBox.getEditor().textProperty().isNotEmpty());
        comboBox.getEditor().textProperty().addListener((observable, oldValue, newValue) -> executeRule(getRule(), comboBox.getEditor().getText()));
        comboBox.getEditor().setEditable(false);
        contentProperty().bind(comboBox.getEditor().textProperty());
        customControls.add(this);
    }

    public Collection<T> getItems() {
        return comboBox.getItems();
    }

    public void setItems(Collection<T> list) {
        comboBox.getItems().addAll(list);
        comboBox.getSelectionModel().selectFirst();
    }

    public ComboBox<T> getComboBox() {
        return comboBox;
    }

    public void setComboBox(ComboBox<T> comboBox) {
        this.comboBox = comboBox;
    }

    @Override
    public void clear() {
        comboBox.getSelectionModel().selectFirst();
    }
}
