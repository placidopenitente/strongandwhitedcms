package com.strongandwhite.customcontrols;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.time.LocalDate;
import java.util.ArrayList;

public class CustomDatePicker extends CustomControl {
    private DatePicker dateField;
    private Label textLabel;
    private SimpleIntegerProperty age;
    private LocalDate defaultDate;

    public CustomDatePicker(HBox control, ArrayList<CustomControl> customControls) {
        age = new SimpleIntegerProperty();
        textLabel = (Label) ((VBox)control.getChildren().get(0)).getChildren().get(0);
        setErrorLabel((Label) ((VBox)control.getChildren().get(0)).getChildren().get(2));
        dateField = (DatePicker) ((StackPane)((VBox)control.getChildren().get(0)).getChildren().get(1)).getChildren().get(0);
        textLabel.visibleProperty().bind(dateField.getEditor().textProperty().isNotEmpty());
        dateField.getEditor().textProperty().addListener((observable, oldValue, newValue) -> executeRule(getRule(), dateField.getEditor().getText()));
        dateField.setConverter(new DateConverter("yyyy-MM-dd"));
        dateField.getEditor().textProperty().addListener(event->{
            LocalDate now = LocalDate.now();
            LocalDate birthday = dateField.getValue();
            int age = now.getYear()-birthday.getYear();
            if(now.getMonth().getValue()==dateField.getValue().getMonth().getValue()) {
                if(now.getDayOfMonth()<birthday.getDayOfMonth()) {
                    age--;
                }
            }
            else if(now.getMonth().getValue()<dateField.getValue().getMonth().getValue()) {
                age--;
            }
            this.age.set(age);
        });
        contentProperty().bind(dateField.getEditor().textProperty());
        customControls.add(this);
    }

    public SimpleIntegerProperty ageProperty() {
        return age;
    }

    public int getAge() {
        return age.get();
    }

    public void setAge(int age) {
        this.age.set(age);
    }

    public LocalDate getDate() {
        return dateField.getValue();
    }

    public void setDate(LocalDate date) {
        dateField.setValue(date);
    }

    public DatePicker getDateField() {
        return dateField;
    }

    public void setDateField(DatePicker dateField) {
        this.dateField = dateField;
    }

    public LocalDate getDefaultDate() {
        return defaultDate;
    }

    public void setDefaultDate(LocalDate defaultDate) {
        this.defaultDate = defaultDate;
        dateField.setValue(defaultDate);
    }

    public Callback<DatePicker, DateCell> getDayCellFactory() {
        return dateField.getDayCellFactory();
    }

    public void setDayCellFactory(Callback<DatePicker, DateCell> cellFactory) {
        dateField.setDayCellFactory(cellFactory);
    }

    @Override
    public void clear() {
        dateField.setValue(LocalDate.now().minusYears(18));
    }
}
