package com.strongandwhite.customcontrols;

import javafx.util.StringConverter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateConverter extends StringConverter<LocalDate> {

    private DateTimeFormatter formatter;

    public DateConverter(String pattern) {
        formatter = DateTimeFormatter.ofPattern(pattern);
    }

    @Override
    public String toString(LocalDate localDate) {
        return formatter.format(localDate);
    }

    @Override
    public LocalDate fromString(String text) {
        return LocalDate.parse(text, formatter);
    }
}
