package com.strongandwhite.customcontrols;

import com.strongandwhite.rules.Rule;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class CustomTextField extends CustomControl {
    private TextField textField;
    private Label textLabel;
    private Button clearButton;

    public CustomTextField(HBox control, ArrayList<CustomControl> customControls) {
        setContainer(control);
        textLabel = (Label) ((VBox)control.getChildren().get(0)).getChildren().get(0);
        setErrorLabel((Label) ((VBox)control.getChildren().get(0)).getChildren().get(2));
        textField = (TextField) ((StackPane)((VBox)control.getChildren().get(0)).getChildren().get(1)).getChildren().get(0);
        clearButton = (Button) ((StackPane)((VBox)control.getChildren().get(0)).getChildren().get(1)).getChildren().get(1);
        textLabel.visibleProperty().bind(textField.textProperty().isNotEmpty());
        clearButton.visibleProperty().bind(textField.textProperty().isNotEmpty().and(textField.focusedProperty()).and(textField.editableProperty()));
        clearButton.setOnAction(event -> textField.clear());
        textField.textProperty().addListener(event -> executeRule(getRule(), textField.getText()));
        contentProperty().bind(textField.textProperty());
        customControls.add(this);
    }

    public StringProperty textProperty() {
        return textField.textProperty();
    }

    public BooleanProperty editableProperty() {
        return textField.editableProperty();
    }

    public String getText() {
        return textField.getText();
    }

    public void setText(String text) {
        textField.setText(text);
    }

    public TextField getTextField() {
        return textField;
    }

    public void setTextField(TextField textField) {
        this.textField = textField;
    }

    @Override
    public void clear() {
        textField.clear();
    }
}
