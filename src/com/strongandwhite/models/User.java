package com.strongandwhite.models;

import javafx.beans.property.SimpleStringProperty;

public class User extends Person {
    private SimpleStringProperty accountID;
    private SimpleStringProperty status;
    private SimpleStringProperty password;
    private SimpleStringProperty accountType;
    private SimpleStringProperty position;
    private SimpleStringProperty recoveryQuestion1;
    private SimpleStringProperty recoveryQuestion2;
    private SimpleStringProperty recoveryQuestion3;
    private SimpleStringProperty recoveryAnswer1;
    private SimpleStringProperty recoveryAnswer2;
    private SimpleStringProperty recoveryAnswer3;

    public User() {
        accountID = new SimpleStringProperty("");
        status = new SimpleStringProperty("");
        password = new SimpleStringProperty("");
        accountType = new SimpleStringProperty("");
        position = new SimpleStringProperty("");
        recoveryQuestion1 = new SimpleStringProperty("");
        recoveryQuestion2 = new SimpleStringProperty("");
        recoveryQuestion3 = new SimpleStringProperty("");
        recoveryAnswer1 = new SimpleStringProperty("");
        recoveryAnswer2 = new SimpleStringProperty("");
        recoveryAnswer3 = new SimpleStringProperty("");
    }

    public String getAccountID() {
        return accountID.get();
    }

    public SimpleStringProperty accountIDProperty() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID.set(accountID);
    }

    public String getStatus() {
        return status.get();
    }

    public SimpleStringProperty statusProperty() {
        return status;
    }

    public void setStatus(String status) {
        this.status.set(status);
    }

    public String getPassword() {
        return password.get();
    }

    public SimpleStringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public String getAccountType() {
        return accountType.get();
    }

    public SimpleStringProperty accountTypeProperty() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType.set(accountType);
    }

    public String getPosition() {
        return position.get();
    }

    public SimpleStringProperty positionProperty() {
        return position;
    }

    public void setPosition(String position) {
        this.position.set(position);
    }

    public String getRecoveryQuestion1() {
        return recoveryQuestion1.get();
    }

    public SimpleStringProperty recoveryQuestion1Property() {
        return recoveryQuestion1;
    }

    public void setRecoveryQuestion1(String recoveryQuestion1) {
        this.recoveryQuestion1.set(recoveryQuestion1);
    }

    public String getRecoveryQuestion2() {
        return recoveryQuestion2.get();
    }

    public SimpleStringProperty recoveryQuestion2Property() {
        return recoveryQuestion2;
    }

    public void setRecoveryQuestion2(String recoveryQuestion2) {
        this.recoveryQuestion2.set(recoveryQuestion2);
    }

    public String getRecoveryQuestion3() {
        return recoveryQuestion3.get();
    }

    public SimpleStringProperty recoveryQuestion3Property() {
        return recoveryQuestion3;
    }

    public void setRecoveryQuestion3(String recoveryQuestion3) {
        this.recoveryQuestion3.set(recoveryQuestion3);
    }

    public String getRecoveryAnswer1() {
        return recoveryAnswer1.get();
    }

    public SimpleStringProperty recoveryAnswer1Property() {
        return recoveryAnswer1;
    }

    public void setRecoveryAnswer1(String recoveryAnswer1) {
        this.recoveryAnswer1.set(recoveryAnswer1);
    }

    public String getRecoveryAnswer2() {
        return recoveryAnswer2.get();
    }

    public SimpleStringProperty recoveryAnswer2Property() {
        return recoveryAnswer2;
    }

    public void setRecoveryAnswer2(String recoveryAnswer2) {
        this.recoveryAnswer2.set(recoveryAnswer2);
    }

    public String getRecoveryAnswer3() {
        return recoveryAnswer3.get();
    }

    public SimpleStringProperty recoveryAnswer3Property() {
        return recoveryAnswer3;
    }

    public void setRecoveryAnswer3(String recoveryAnswer3) {
        this.recoveryAnswer3.set(recoveryAnswer3);
    }
}
