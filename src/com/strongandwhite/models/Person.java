package com.strongandwhite.models;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.image.Image;

import java.util.Date;

public abstract class Person {
    private SimpleStringProperty firstName;
    private SimpleStringProperty middleName;
    private SimpleStringProperty lastName;
    private SimpleStringProperty birthdate;
    private SimpleIntegerProperty age;
    private SimpleStringProperty gender;
    private SimpleStringProperty address;
    private SimpleStringProperty contactNo;
    private SimpleStringProperty emailAddress;
    private SimpleStringProperty nationality;
    private SimpleStringProperty religion;
    private SimpleStringProperty image;
    private SimpleDoubleProperty saveProgress;
    private SimpleIntegerProperty accountNumber;
    private Image userPhoto;

    public Person() {
        firstName = new SimpleStringProperty("");
        middleName = new SimpleStringProperty("");
        lastName = new SimpleStringProperty("");
        birthdate = new SimpleStringProperty("");
        age = new SimpleIntegerProperty();
        gender = new SimpleStringProperty("");
        address = new SimpleStringProperty("");
        contactNo = new SimpleStringProperty("");
        emailAddress = new SimpleStringProperty("");
        nationality = new SimpleStringProperty("");
        religion = new SimpleStringProperty("");
        image = new SimpleStringProperty("");
        saveProgress = new SimpleDoubleProperty(0);
        accountNumber = new SimpleIntegerProperty(-1);
    }

    public String getFirstName() {
        return firstName.get();
    }

    public SimpleStringProperty firstNameProperty() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getMiddleName() {
        return middleName.get();
    }

    public SimpleStringProperty middleNameProperty() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName.set(middleName);
    }

    public String getLastName() {
        return lastName.get();
    }

    public SimpleStringProperty lastNameProperty() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public String getBirthdate() {
        return birthdate.get();
    }

    public SimpleStringProperty birthdateProperty() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate.set(birthdate);
    }

    public int getAge() {
        return age.get();
    }

    public SimpleIntegerProperty ageProperty() {
        return age;
    }

    public void setAge(int age) {
        this.age.set(age);
    }

    public String getGender() {
        return gender.get();
    }

    public SimpleStringProperty genderProperty() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender.set(gender);
    }

    public String getAddress() {
        return address.get();
    }

    public SimpleStringProperty addressProperty() {
        return address;
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public String getContactNo() {
        return contactNo.get();
    }

    public SimpleStringProperty contactNoProperty() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo.set(contactNo);
    }

    public String getEmailAddress() {
        return emailAddress.get();
    }

    public SimpleStringProperty emailAddressProperty() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress.set(emailAddress);
    }

    public String getNationality() {
        return nationality.get();
    }

    public SimpleStringProperty nationalityProperty() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality.set(nationality);
    }

    public String getReligion() {
        return religion.get();
    }

    public SimpleStringProperty religionProperty() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion.set(religion);
    }

    public String getImage() {
        return image.get();
    }

    public SimpleStringProperty imageProperty() {
        return image;
    }

    public void setImage(String image) {
        this.image.set(image);
    }

    public double getSaveProgress() {
        return saveProgress.get();
    }

    public SimpleDoubleProperty saveProgressProperty() {
        return saveProgress;
    }

    public void setSaveProgress(double saveProgress) {
        this.saveProgress.set(saveProgress);
    }

    public int getAccountNumber() {
        return accountNumber.get();
    }

    public SimpleIntegerProperty accountNumberProperty() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber.set(accountNumber);
    }

    public Image getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(Image userPhoto) {
        this.userPhoto = userPhoto;
    }
}
