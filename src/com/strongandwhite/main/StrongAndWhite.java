package com.strongandwhite.main;

import com.strongandwhite.models.User;
import com.strongandwhite.pages.dentistadnstaff.AddNewAccount;
import com.strongandwhite.pages.dentistadnstaff.UserAccounts;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class StrongAndWhite extends Application {
    private MainWindow mainWindow;
    private User activeUser;
    private DatabaseConnection databaseConnection;
    private FTPConnection ftpConnection;

    @Override
    public void start(Stage primaryStage) {
        mainWindow = new MainWindow();
        activeUser = new User();
        activeUser.setImage("file:/C:/Users/Potenciano/Pictures/2017-06-12/153.JPG");
        activeUser.setAccountID("Administrator");
        databaseConnection = new DatabaseConnection();
        ftpConnection = new FTPConnection();
        databaseConnection.setFtpConnection(ftpConnection);
        mainWindow.setDatabaseConnection(databaseConnection);
        mainWindow.setActiveUser(activeUser);
        User user = new User();
        UserAccounts userAccounts = new UserAccounts(mainWindow);
        databaseConnection.setUserAccounts(userAccounts);
        mainWindow.setActiveUser(activeUser);
        mainWindow.gotoDentistAndStaff(userAccounts);
        //mainWindow.gotoAddNewAccount(new AddNewAccount(user, mainWindow, activeUser));
        Scene scene = new Scene(mainWindow);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String... args) {
        launch(args);
    }
}
