package com.strongandwhite.main;

import com.strongandwhite.models.User;
import com.strongandwhite.pages.dentistadnstaff.UserAccounts;
import javafx.scene.image.Image;

import java.io.File;
import java.sql.*;

public class DatabaseConnection {
    private Connection connection;
    private FTPConnection ftpConnection;
    private Thread saveUserThread;
    private Thread loadUsersThread;
    private User user;
    private User userCopy;
    private User activeUser;
    private UserAccounts userAccounts;

    public DatabaseConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            connection = createConnection();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        saveUserThread = new Thread(this::saveUser);
        loadUsersThread = new Thread(this::loadUsers);
    }

    public Connection createConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/strongwhite_database","strongwhite","strongwhite");
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void addUser(User user, User activeUser, User userCopy) {
        if(!saveUserThread.isAlive()) {
            this.user = user;
            this.userCopy = userCopy;
            this.activeUser = activeUser;
            saveUserThread = new Thread(this::saveUser);
            saveUserThread.setDaemon(true);
            saveUserThread.start();
        }
    }

    public void saveUser() {
        user.setSaveProgress(0.1);
        try {
            PreparedStatement checkStatement = connection.prepareStatement("SELECT user_no FROM strongwhite_useraccounts WHERE user_accountid=? LIMIT 1");
            checkStatement.setString(1, userCopy.getAccountID());
            ResultSet checkResultSet = checkStatement.executeQuery();
            user.setSaveProgress(0.2);
            if(checkResultSet.next()) {
                PreparedStatement updateStatement = connection.prepareStatement("UPDATE strongwhite_useraccounts " +
                        "SET " +
                        "user_accountid=?, " +
                        "user_password=?, " +
                        "user_accounttype=?, " +
                        "user_position=?, " +
                        "user_image=?," +
                        "user_firstname=?," +
                        "user_middlename=?," +
                        "user_lastname=?," +
                        "user_gender=?," +
                        "user_birthdate=?," +
                        "user_address=?," +
                        "user_contactno=?," +
                        "user_emailaddress=?," +
                        "user_nationality=?," +
                        "user_religion=?," +
                        "user_question1=?," +
                        "user_answer1=?," +
                        "user_question2=?," +
                        "user_answer2=?," +
                        "user_question3=?," +
                        "user_answer3=?," +
                        "user_updated=NOW(), " +
                        "user_updatedby=?" +
                        "WHERE user_no=?");
                user.setSaveProgress(0.3);
                updateStatement.setString(1, user.getAccountID());
                updateStatement.setString(2, user.getPassword());
                updateStatement.setString(3, user.getAccountType());
                updateStatement.setString(4, user.getPosition());
                updateStatement.setString(5, user.getImage());
                updateStatement.setString(6, user.getFirstName());
                updateStatement.setString(7, user.getMiddleName());
                updateStatement.setString(8, user.getLastName());
                updateStatement.setString(9, user.getGender());
                updateStatement.setString(10, user.getBirthdate());
                updateStatement.setString(11, user.getAddress());
                updateStatement.setString(12, user.getContactNo());
                updateStatement.setString(13, user.getEmailAddress());
                updateStatement.setString(14, user.getNationality());
                updateStatement.setString(15, user.getReligion());
                updateStatement.setString(16, user.getRecoveryQuestion1());
                updateStatement.setString(17, user.getRecoveryAnswer1());
                updateStatement.setString(18, user.getRecoveryQuestion2());
                updateStatement.setString(19, user.getRecoveryAnswer2());
                updateStatement.setString(20, user.getRecoveryQuestion3());
                updateStatement.setString(21, user.getRecoveryAnswer3());
                updateStatement.setString(22, activeUser.getAccountID());
                updateStatement.setString(23, checkResultSet.getString("user_no"));
                user.setSaveProgress(0.5);
                updateStatement.executeUpdate();
                user.setSaveProgress(0.7);
            }
            else {
                PreparedStatement insertStatement = connection.prepareStatement("INSERT INTO strongwhite_useraccounts VALUES (" +
                        "NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, NOW(), NOW(), ?)");
                user.setSaveProgress(0.3);
                insertStatement.setString(1, "offline");
                insertStatement.setString(2, user.getAccountID());
                insertStatement.setString(3, user.getPassword());
                insertStatement.setString(4, user.getAccountType());
                insertStatement.setString(5, user.getPosition());
                insertStatement.setString(6, "none");
                insertStatement.setString(7, user.getFirstName());
                insertStatement.setString(8, user.getMiddleName());
                insertStatement.setString(9, user.getLastName());
                insertStatement.setString(10, user.getGender());
                insertStatement.setString(11, user.getBirthdate());
                insertStatement.setString(12, user.getAddress());
                insertStatement.setString(13, user.getContactNo());
                insertStatement.setString(14, user.getEmailAddress());
                insertStatement.setString(15, user.getNationality());
                insertStatement.setString(16, user.getReligion());
                insertStatement.setString(17, user.getRecoveryQuestion1());
                insertStatement.setString(18, user.getRecoveryAnswer1());
                insertStatement.setString(19, user.getRecoveryQuestion2());
                insertStatement.setString(20, user.getRecoveryAnswer2());
                insertStatement.setString(21, user.getRecoveryQuestion3());
                insertStatement.setString(22, user.getRecoveryAnswer3());
                insertStatement.setString(23, activeUser.getAccountID());
                user.setSaveProgress(0.5);
                insertStatement.executeUpdate();
                user.setSaveProgress(0.7);
            }

            PreparedStatement getAccountNo = connection.prepareStatement("SELECT user_no from strongwhite_useraccounts WHERE user_accountID=?");
            getAccountNo.setString(1, user.getAccountID());
            ResultSet resultSet = getAccountNo.executeQuery();
            String accountNo = "";
            if(resultSet.next()) {
                accountNo = resultSet.getString("user_no");
                user.setImage(resultSet.getString("user_no")+".jpg");
            }

            PreparedStatement uploadPicture = connection.prepareStatement("UPDATE strongwhite_useraccounts SET user_image=? WHERE user_no=?");
            uploadPicture.setString(1, user.getImage());
            uploadPicture.setString(2, accountNo);
            uploadPicture.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        user.setSaveProgress(0.8);
        ftpConnection.uploadUserPicture(user.getImage());
        user.setSaveProgress(1);

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        user.setSaveProgress(0);
    }

    public void loadUsers() {
        int count = 0;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT('user_no') AS 'total_users' FROM strongwhite_useraccounts");
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            count = Integer.parseInt(resultSet.getString("total_users"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM strongwhite_useraccounts");
            ResultSet resultSet = preparedStatement.executeQuery();
            for(int i=0; i<count; i++) {
                resultSet.next();
                User user = new User();
                user.setAccountID(resultSet.getString("user_accountID"));
                user.setFirstName(resultSet.getString("user_firstname"));
                user.setMiddleName(resultSet.getString("user_middlename"));
                user.setLastName(resultSet.getString("user_lastname"));
                user.setGender(resultSet.getString("user_gender"));
                user.setContactNo(resultSet.getString("user_contactno"));
                user.setPosition(resultSet.getString("user_position"));
                user.setAccountType(resultSet.getString("user_accounttype"));
                user.setBirthdate(resultSet.getString("user_birthdate"));
                user.setAddress(resultSet.getString("user_address"));
                user.setStatus(resultSet.getString("user_status"));
                user.setAccountNumber(Integer.parseInt(resultSet.getString("user_no")));
                user.setReligion(resultSet.getString("user_religion"));
                user.setNationality(resultSet.getString("user_nationality"));
                user.setImage(resultSet.getString("user_image"));
                user.setPassword(resultSet.getString("user_password"));
                user.setEmailAddress(resultSet.getString("user_emailaddress"));
                user.setRecoveryQuestion1(resultSet.getString("user_question1"));
                user.setRecoveryQuestion2(resultSet.getString("user_question2"));
                user.setRecoveryQuestion3(resultSet.getString("user_question3"));
                user.setRecoveryAnswer1(resultSet.getString("user_answer1"));
                user.setRecoveryAnswer2(resultSet.getString("user_answer2"));
                user.setRecoveryAnswer3(resultSet.getString("user_answer3"));
                ftpConnection.downloadUserPicture(resultSet.getString("user_image"));
                File file = new File("temp/"+resultSet.getString("user_image"));
                if(file.isFile()&&file.exists()) {
                    user.setUserPhoto(new Image("file:temp/"+resultSet.getString("user_image"), 0, 200, true, true, true));
                }
                else {
                    user.setUserPhoto(new Image("resources/images/Male User_100px.png"));
                }
                userAccounts.getUsers().add(user);
                userAccounts.getPageTitle().setProgress((float)(i+1)/count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        userAccounts.getPageTitle().setProgress(0);
    }

    public FTPConnection getFtpConnection() {
        return ftpConnection;
    }

    public void setFtpConnection(FTPConnection ftpConnection) {
        this.ftpConnection = ftpConnection;
    }

    public User getUserCopy() {
        return userCopy;
    }

    public void setUserCopy(User userCopy) {
        this.userCopy = userCopy;
    }

    public void loadAllUsers() {
        if(!loadUsersThread.isAlive()) {
            loadUsersThread = new Thread(this::loadUsers);
            loadUsersThread.setDaemon(true);
            loadUsersThread.start();
        }
    }

    public void setUserAccounts(UserAccounts userAccounts) {
        this.userAccounts = userAccounts;
    }

    public UserAccounts getUserAccounts() {
        return userAccounts;
    }
}
