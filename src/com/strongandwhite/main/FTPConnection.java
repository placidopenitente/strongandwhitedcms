package com.strongandwhite.main;

import javafx.scene.image.Image;
import org.apache.commons.net.ftp.FTPClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

public class FTPConnection {
    private FTPClient ftpClient;
    private FileInputStream fileInputStream;

    public  FTPConnection() {
        ftpClient = new FTPClient();
    }

    public void uploadUserPicture(String image) {
        try {
            ftpClient.connect("localhost", 21);
            ftpClient.login("potenciano","strongwhite");
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            fileInputStream = new FileInputStream("temp/temporary_image.jpg");
            ftpClient.storeFile("strongwhite_useraccounts/profile_pictures/"+image, fileInputStream);
            ftpClient.logout();
            ftpClient.disconnect();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void downloadUserPicture(String image) {
        try {
            ftpClient.connect("localhost", 21);
            ftpClient.login("potenciano","strongwhite");
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            FileOutputStream fileOutputStream = new FileOutputStream("temp/"+image);
            if(!ftpClient.retrieveFile("strongwhite_useraccounts/profile_pictures/"+image, fileOutputStream)) {
                ftpClient.logout();
                ftpClient.disconnect();
                fileOutputStream.close();
                File file = new File("temp/"+image);
                Files.delete(file.toPath());
            }
            ftpClient.logout();
            ftpClient.disconnect();
            fileOutputStream.close();
        } catch (Exception e) {
        }
    }
}
