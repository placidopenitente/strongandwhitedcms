package com.strongandwhite.main;

import com.strongandwhite.models.User;
import com.strongandwhite.pages.dentistadnstaff.AddNewAccount;
import com.strongandwhite.pages.dentistadnstaff.UserAccounts;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Circle;

import java.io.IOException;

public class MainWindow extends BorderPane {
    private DatabaseConnection databaseConnection;
    private User activeUser;

    @FXML private ScrollPane contentPane;
    @FXML private StackPane activePagePane;
    @FXML private ImageView activeUserImage;

    public MainWindow() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("MainWindow.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void gotoAddNewAccount(AddNewAccount addNewAccount) {
        contentPane.setContent(addNewAccount);
        addNewAccount.setContentPane(contentPane);
    }

    public StackPane getActivePagePane() {
        return activePagePane;
    }

    public void setActivePagePane(StackPane activePagePane) {
        this.activePagePane = activePagePane;
    }

    public DatabaseConnection getDatabaseConnection() {
        return databaseConnection;
    }

    public void setDatabaseConnection(DatabaseConnection databaseConnection) {
        this.databaseConnection = databaseConnection;
    }

    public User getActiveUser() {
        return activeUser;
    }

    public void setActiveUser(User activeUser) {
        this.activeUser = activeUser;
        Circle clip = new Circle(activeUserImage.getFitHeight()/2,activeUserImage.getFitHeight()/2, activeUserImage.getFitHeight()/2);
        activeUserImage.setClip(clip);
        activeUserImage.setImage(new Image(activeUser.getImage(),200,200,true,true,true));
    }

    public void gotoDentistAndStaff(UserAccounts userAccounts) {
        contentPane.setContent(userAccounts);
        userAccounts.setContentPane(contentPane);
    }
}
