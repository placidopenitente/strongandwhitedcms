package com.strongandwhite.pages.dentistadnstaff;

import com.strongandwhite.models.User;
import com.strongandwhite.pages.Page;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;

import java.io.IOException;

public class ReviewAccount extends Page {
    private AddNewAccount addNewAccount;
    private User user;

    @FXML private Label accountID;
    @FXML private Label accountType;
    @FXML private Label position;
    @FXML private Label completeName;
    @FXML private Label gender;
    @FXML private Label birthdate;
    @FXML private Label age;
    @FXML private Label address;
    @FXML private Label contactNo;
    @FXML private Label emailAddress;
    @FXML private Label nationality;
    @FXML private Label religion;
    @FXML private Label question1;
    @FXML private Label answer1;
    @FXML private Label question2;
    @FXML private Label answer2;
    @FXML private Label question3;
    @FXML private Label answer3;
    @FXML private Button saveButton;
    @FXML private Button cancelButton;
    @FXML private ImageView image;

    public ReviewAccount(AddNewAccount addNewAccount) {
        this.user = addNewAccount.getUser();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ReviewAccount.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        image.imageProperty().bind(addNewAccount.getImage().imageProperty());
        accountID.textProperty().bind(user.accountIDProperty());
        accountType.textProperty().bind(user.accountTypeProperty());
        position.textProperty().bind(user.positionProperty());
        completeName.textProperty().bind(user.lastNameProperty().concat(", ").concat(user.firstNameProperty()).concat(" ").concat(user.middleNameProperty()));
        gender.textProperty().bind(user.genderProperty());
        birthdate.textProperty().bind(user.birthdateProperty());
        age.textProperty().bind(user.ageProperty().asString().concat(" years old"));
        address.textProperty().bind(user.addressProperty());
        contactNo.textProperty().bind(user.contactNoProperty());
        emailAddress.textProperty().bind(user.emailAddressProperty());
        nationality.textProperty().bind(user.nationalityProperty());
        religion.textProperty().bind(user.religionProperty());
        question1.textProperty().bind(user.recoveryQuestion1Property());
        answer1.textProperty().bind(user.recoveryAnswer1Property());
        question2.textProperty().bind(user.recoveryQuestion2Property());
        answer2.textProperty().bind(user.recoveryAnswer2Property());
        question3.textProperty().bind(user.recoveryQuestion3Property());
        answer3.textProperty().bind(user.recoveryAnswer3Property());
        cancelButton.setOnAction(event -> addNewAccount.goBack());
        saveButton.setOnAction(event -> addNewAccount.saveUser());
    }
}
