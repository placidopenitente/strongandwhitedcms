package com.strongandwhite.pages.dentistadnstaff;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.StackPane;

import java.io.IOException;

public class UserAccountsItem extends StackPane {
    public UserAccountsItem() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("UserAccountsItem.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
