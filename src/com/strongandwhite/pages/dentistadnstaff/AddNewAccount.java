package com.strongandwhite.pages.dentistadnstaff;

import com.strongandwhite.customcontrols.*;
import com.strongandwhite.main.FTPConnection;
import com.strongandwhite.main.MainWindow;
import com.strongandwhite.models.User;
import com.strongandwhite.pages.Page;
import com.strongandwhite.pages.PageTitle;
import com.strongandwhite.rules.*;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;

public class AddNewAccount extends Page {
    private ArrayList<String> gender;
    private ArrayList<String> type;
    private ArrayList<String> position;
    private User user;
    private User userCopy;
    private User activeUser;
    private ReviewAccount reviewAccount;

    @FXML private ImageView image;

    @FXML private HBox firstNameHBox;
    private CustomTextField firstNameTextField;

    @FXML private HBox middleNameHBox;
    private CustomTextField middleNameTextField;

    @FXML private HBox lastNameHBox;
    private CustomTextField lastNameTextField;

    @FXML private HBox ageHBox;
    private CustomTextField ageTextField;

    @FXML private HBox genderHBox;
    private CustomComboBox<String> genderComboBox;

    @FXML private HBox addressHBox;
    private CustomTextField addressTextField;

    @FXML private HBox contactHBox;
    private CustomTextField contactTextField;

    @FXML private HBox nationalityHBox;
    private CustomTextField nationalityTextField;

    @FXML private HBox religionHBox;
    private CustomTextField religionTextField;

    @FXML private HBox emailAddressHBox;
    private CustomTextField emailAddressTextField;

    @FXML private HBox birthdateHBox;
    private CustomDatePicker birtdateDatePicker;

    @FXML private HBox accountIDHBox;
    private CustomTextField accountIDTextField;

    @FXML private HBox passwordHBox;
    private CustomPasswordField passwordTextField;

    @FXML private HBox confirmPasswordHBox;
    private CustomPasswordField confirmPasswordTextField;

    @FXML private HBox accountTypeHBox;
    private CustomComboBox<String> accountTypeComboBox;

    @FXML private HBox positionHBox;
    private CustomComboBox<String> positionComboBox;

    @FXML private HBox question1HBox;
    private CustomTextField question1TextField;

    @FXML private HBox question2HBox;
    private CustomTextField question2TextField;

    @FXML private HBox question3HBox;
    private CustomTextField question3TextField;

    @FXML private HBox answer1HBox;
    private CustomTextField answer1TextField;

    @FXML private HBox answer2HBox;
    private CustomTextField answer2TextField;

    @FXML private HBox answer3HBox;
    private CustomTextField answer3TextField;

    @FXML private Button clearButton;
    @FXML private Button proceedButton;
    @FXML private Button uploadButton;
    @FXML private ProgressIndicator uploadProgress;
    @FXML private Circle clip;

    private ScrollPane scrollPane;
    private ScrollPane contentPane;
    private MainWindow mainWindow;
    private FileChooser fileChooser;
    private FTPConnection ftpConnection;

    public AddNewAccount(User user, MainWindow mainWindow, User activeUser) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("AddNewAccount.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.user = user;
        this.activeUser = activeUser;
        userCopy = new User();
        this.mainWindow = mainWindow;
        reviewAccount = new ReviewAccount(this);
        ftpConnection = mainWindow.getDatabaseConnection().getFtpConnection();

        userCopy.setImage(user.getImage());
        userCopy.setFirstName(user.getFirstName());
        userCopy.setMiddleName(user.getMiddleName());
        userCopy.setLastName(user.getLastName());
        userCopy.setBirthdate(user.getBirthdate());
        userCopy.setGender(user.getGender());
        userCopy.setFirstName(user.getFirstName());
        userCopy.setAddress(user.getAddress());
        userCopy.setContactNo(user.getContactNo());
        userCopy.setNationality(user.getNationality());
        userCopy.setReligion(user.getReligion());
        userCopy.setEmailAddress(user.getEmailAddress());
        userCopy.setAccountID(user.getAccountID());
        userCopy.setPassword(user.getPassword());
        userCopy.setAccountType(user.getAccountType());
        userCopy.setPosition(user.getPosition());
        userCopy.setRecoveryQuestion1(user.getRecoveryQuestion1());
        userCopy.setRecoveryQuestion2(user.getRecoveryQuestion2());
        userCopy.setRecoveryQuestion3(user.getRecoveryQuestion3());
        userCopy.setRecoveryAnswer1(user.getRecoveryAnswer1());
        userCopy.setRecoveryAnswer2(user.getRecoveryAnswer2());
        userCopy.setRecoveryAnswer3(user.getRecoveryAnswer3());

        fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Image Files", "*.jpg");
        fileChooser.getExtensionFilters().add(extensionFilter);

        customControls = new ArrayList<>();
        pageTitle = new PageTitle();
        pageTitle.setProgress(0);
        if(user.getAccountNumber()<0) {
            pageTitle.setTitle("Add New Account");
            pageTitle.setIcon(new Image("resources/images/Add User Male_36px.png"));
        }
        else {
            pageTitle.setTitle("Update Account");
            pageTitle.setIcon(new Image("resources/images/Edit User Male_36px.png"));
        }

        pageTitle.getProgressBar().progressProperty().bind(user.saveProgressProperty());

        gender = new ArrayList<>();
        gender.add("Male");
        gender.add("Female");

        type = new ArrayList<>();
        type.add("Administrator");
        type.add("Standard");
        type.add("Limited");

        position = new ArrayList<>();
        position.add("Dentist");
        position.add("Staff");

        firstNameTextField = new CustomTextField(firstNameHBox, customControls);
        middleNameTextField = new CustomTextField(middleNameHBox, customControls);
        lastNameTextField = new CustomTextField(lastNameHBox, customControls);
        birtdateDatePicker = new CustomDatePicker(birthdateHBox, customControls);
        ageTextField = new CustomTextField(ageHBox, customControls);
        genderComboBox = new CustomComboBox<>(genderHBox, customControls);
        addressTextField = new CustomTextField(addressHBox, customControls);
        contactTextField = new CustomTextField(contactHBox, customControls);
        nationalityTextField = new CustomTextField(nationalityHBox, customControls);
        religionTextField = new CustomTextField(religionHBox, customControls);
        emailAddressTextField = new CustomTextField(emailAddressHBox, customControls);
        accountIDTextField = new CustomTextField(accountIDHBox, customControls);
        passwordTextField = new CustomPasswordField(passwordHBox, customControls);
        confirmPasswordTextField = new CustomPasswordField(confirmPasswordHBox, customControls);
        accountTypeComboBox = new CustomComboBox<>(accountTypeHBox, customControls);
        positionComboBox = new CustomComboBox<>(positionHBox, customControls);
        question1TextField = new CustomTextField(question1HBox, customControls);
        question2TextField = new CustomTextField(question2HBox, customControls);
        question3TextField = new CustomTextField(question3HBox, customControls);
        answer1TextField = new CustomTextField(answer1HBox, customControls);
        answer2TextField = new CustomTextField(answer2HBox, customControls);
        answer3TextField = new CustomTextField(answer3HBox, customControls);

        firstNameTextField.setRule(new NameRule("First Name"));
        middleNameTextField.setRule(new NameRule("Middle Name"));
        lastNameTextField.setRule(new NameRule("Last Name"));
        birtdateDatePicker.setRule(new DateRule());
        birtdateDatePicker.setDefaultDate(LocalDate.now().minusYears(18));
        birtdateDatePicker.setDayCellFactory(param -> new DateCell() {
            @Override
            public void updateItem(LocalDate item, boolean empty) {
                super.updateItem(item, empty);
                if(item.isBefore(ChronoLocalDate.from(LocalDate.now().minusYears(76).minusMonths(1)))||item.isAfter(ChronoLocalDate.from(LocalDate.now().minusYears(18).plusMonths(1)))) {
                    setDisable(true);
                }
            }
        });
        ageTextField.setRule(new NumbersOnlyRule());
        genderComboBox.setRule(new WordRule("Gender"));
        genderComboBox.setItems(gender);
        addressTextField.setRule(new AddressRule());
        contactTextField.setRule(new ContactNumberRule());
        nationalityTextField.setRule(new NameRule("Nationality"));
        religionTextField.setRule(new NameRule("Religion"));
        emailAddressTextField.setRule(new EmailAddressRule());
        accountIDTextField.setRule(new AccountIDRule());
        passwordTextField.setRule(new PasswordRule());
        passwordTextField.textProperty().addListener(event-> checkSimilarity());
        confirmPasswordTextField.setRule(new PasswordRule());
        confirmPasswordTextField.textProperty().addListener(event-> checkSimilarity());
        accountTypeComboBox.setRule(new WordRule("Account Type"));
        accountTypeComboBox.setItems(type);
        accountIDTextField.textProperty().addListener(event->checkAccountID());
        positionComboBox.setRule(new WordRule("Position"));
        positionComboBox.setItems(position);
        question1TextField.setRule(new WordRule("Question"));
        question2TextField.setRule(new WordRule("Question"));
        question3TextField.setRule(new WordRule("Question"));
        answer1TextField.setRule(new WordRule("Answer"));
        answer2TextField.setRule(new WordRule("Answer"));
        answer3TextField.setRule(new WordRule("Answer"));

        clearButton.setOnAction(event -> resetFields());
        proceedButton.setOnAction(event -> proceedToReview());
        uploadButton.setOnAction(event -> setPhoto());

        loadDefaults();

        user.firstNameProperty().bind(firstNameTextField.textProperty());
        user.middleNameProperty().bind(middleNameTextField.textProperty());
        user.lastNameProperty().bind(lastNameTextField.textProperty());
        user.birthdateProperty().bind(birtdateDatePicker.getDateField().getEditor().textProperty());
        user.genderProperty().bind(genderComboBox.getComboBox().getEditor().textProperty());
        user.addressProperty().bind(addressTextField.textProperty());
        user.contactNoProperty().bind(contactTextField.textProperty());
        user.nationalityProperty().bind(nationalityTextField.textProperty());
        user.religionProperty().bind(religionTextField.textProperty());
        user.emailAddressProperty().bind(emailAddressTextField.textProperty());
        user.accountIDProperty().bind(accountIDTextField.textProperty());
        user.passwordProperty().bind(passwordTextField.textProperty());
        user.accountTypeProperty().bind(accountTypeComboBox.getComboBox().getEditor().textProperty());
        user.positionProperty().bind(positionComboBox.getComboBox().getEditor().textProperty());
        user.recoveryQuestion1Property().bind(question1TextField.textProperty());
        user.recoveryQuestion2Property().bind(question2TextField.textProperty());
        user.recoveryQuestion3Property().bind(question3TextField.textProperty());
        user.recoveryAnswer1Property().bind(answer1TextField.textProperty());
        user.recoveryAnswer2Property().bind(answer2TextField.textProperty());
        user.recoveryAnswer3Property().bind(answer3TextField.textProperty());
        user.ageProperty().bind(birtdateDatePicker.ageProperty());
        ageTextField.textProperty().bind(user.ageProperty().asString());

        answer1TextField.editableProperty().bind(question1TextField.textProperty().isNotEmpty());
        answer1TextField.editableProperty().addListener(event-> {
            if(question1TextField.textProperty().getValue().trim().isEmpty()) answer1TextField.clear();
        });

        answer2TextField.editableProperty().bind(question2TextField.textProperty().isNotEmpty());
        answer2TextField.editableProperty().addListener(event-> {
            if(question2TextField.textProperty().getValue().trim().isEmpty()) answer2TextField.clear();
        });

        answer3TextField.editableProperty().bind(question3TextField.textProperty().isNotEmpty());
        answer3TextField.editableProperty().addListener(event-> {
            if(question3TextField.textProperty().getValue().trim().isEmpty()) answer3TextField.clear();
        });

        mainWindow.getActivePagePane().getChildren().add(getPageTitle());

        scrollPane = (ScrollPane) mainWindow.getActivePagePane().getChildren().get(0);
        scrollPane.vvalueProperty().addListener(event-> scrollPane.requestFocus());
    }

    private void checkSimilarity() {
        if(!passwordTextField.getText().equals(confirmPasswordTextField.getText())) {
            confirmPasswordTextField.getErrorLabel().setText("Passwords do not match.");
        }
    }

    private void loadDefaults() {
        resetImage();
        if(!user.getFirstName().equals("")) firstNameTextField.setText(user.getFirstName());
        if(!user.getMiddleName().equals("")) middleNameTextField.setText(user.getMiddleName());
        if(!user.getLastName().equals("")) lastNameTextField.setText(user.getLastName());
        if(!user.getBirthdate().equals("")) birtdateDatePicker.getDateField().setValue(LocalDate.parse(user.getBirthdate()));
        genderComboBox.getComboBox().getSelectionModel().select(user.getGender());
        if(!user.getAddress().equals("")) addressTextField.setText(user.getAddress());
        if(!user.getContactNo().equals("")) contactTextField.setText(user.getContactNo());
        if(!user.getNationality().equals("")) nationalityTextField.setText(user.getNationality());
        if(!user.getReligion().equals("")) religionTextField.setText(user.getReligion());
        if(!user.getEmailAddress().equals("")) emailAddressTextField.setText(user.getEmailAddress());
        if(!user.getAccountID().equals("")) accountIDTextField.setText(user.getAccountID());
        if(!user.getPassword().equals("")) passwordTextField.setText(user.getPassword());
        if(!user.getPassword().equals("")) confirmPasswordTextField.setText(user.getPassword());
        accountTypeComboBox.getComboBox().getSelectionModel().select(user.getAccountType());
        positionComboBox.getComboBox().getSelectionModel().select(user.getPosition());
        if(!user.getRecoveryQuestion1().equals("")) question1TextField.setText(user.getRecoveryQuestion1());
        if(!user.getRecoveryQuestion2().equals("")) question2TextField.setText(user.getRecoveryQuestion2());
        if(!user.getRecoveryAnswer3().equals("")) question3TextField.setText(user.getRecoveryQuestion3());
        if(!user.getRecoveryAnswer1().equals("")) answer1TextField.setText(user.getRecoveryAnswer1());
        if(!user.getRecoveryAnswer2().equals("")) answer2TextField.setText(user.getRecoveryAnswer2());
        if(!user.getRecoveryAnswer3().equals("")) answer3TextField.setText(user.getRecoveryAnswer3());
    }

    private void resetImage() {
        File tempFile = new File("temp/temporary_image.jpg");
        image.setClip(null);
        try {
            Files.delete(tempFile.toPath());
        } catch (IOException e) {
        }
        tempFile = new File("temp/"+userCopy.getImage());
        try {
            Files.delete(tempFile.toPath());
        } catch (IOException e) {
        }
        ftpConnection.downloadUserPicture(userCopy.getImage());
        Image photo = new Image("file:temp/"+userCopy.getImage());
        File file = new File("temp/"+userCopy.getImage());
        if(file.isFile()&&file.exists()) {
            double width = 200*photo.getWidth()/photo.getHeight();
            clip = new Circle(width/4, 50, 50);
            image.setClip(clip);
            image.setImage(new Image("file:temp/"+userCopy.getImage(), 0, 200, true, true, true));
        }
        else {
            image.setImage(new Image("resources/images/Male User_100px.png"));
        }
    }

    private void resetFields() {
        resetImage();
        firstNameTextField.setText(userCopy.getFirstName());
        middleNameTextField.setText(userCopy.getMiddleName());
        lastNameTextField.setText(userCopy.getLastName());
        if(!userCopy.getBirthdate().equals("")) birtdateDatePicker.getDateField().setValue(LocalDate.parse(userCopy.getBirthdate()));
        genderComboBox.getComboBox().getSelectionModel().select(userCopy.getGender());
        addressTextField.setText(userCopy.getAddress());
        contactTextField.setText(userCopy.getContactNo());
        nationalityTextField.setText(userCopy.getNationality());
        religionTextField.setText(userCopy.getReligion());
        emailAddressTextField.setText(userCopy.getEmailAddress());
        accountIDTextField.setText(userCopy.getAccountID());
        passwordTextField.setText(userCopy.getPassword());
        confirmPasswordTextField.setText(userCopy.getPassword());
        accountTypeComboBox.getComboBox().getSelectionModel().select(userCopy.getAccountType());
        positionComboBox.getComboBox().getSelectionModel().select(userCopy.getPosition());
        question1TextField.setText(userCopy.getRecoveryQuestion1());
        question2TextField.setText(userCopy.getRecoveryQuestion2());
        question3TextField.setText(userCopy.getRecoveryQuestion3());
        answer1TextField.setText(userCopy.getRecoveryAnswer1());
        answer2TextField.setText(userCopy.getRecoveryAnswer2());
        answer3TextField.setText(userCopy.getRecoveryAnswer3());
    }

    public ImageView getImage() {
        return image;
    }

    private void setPhoto() {
        File tempFile = new File("temp/temporary_image.jpg");
        try {
            Files.delete(tempFile.toPath());
        } catch (IOException e) {
        }
        Image original = image.getImage();
        File file = fileChooser.showOpenDialog(null);
        try {
            Image photo = new Image(file.toURI().toURL().toString());
            double width = 200*photo.getWidth()/photo.getHeight();
            clip = new Circle(width/4, 50, 50);
            image.setClip(clip);
            ImageIO.write(SwingFXUtils.fromFXImage(photo, null),"jpg",new File("temp/temporary_image.jpg"));
            image.setImage(new Image("file:temp/temporary_image.jpg",0,200,true,true,true));
        } catch (Exception e) {
            image.setImage(original);
        }
    }

    private void proceedToReview() {
        for(CustomControl customControl : customControls) {
            if(!customControl.getErrorLabel().getText().equals("")||customControl.getContent().equals("")) {
                return;
            }
        }
        contentPane.setContent(reviewAccount);
        contentPane.setVvalue(0);
    }

    public ScrollPane getContentPane() {
        return contentPane;
    }

    public void setContentPane(ScrollPane contentPane) {
        this.contentPane = contentPane;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void goBack() {
        contentPane.setContent(this);
        contentPane.setVvalue(0);
    }

    public void checkAccountID() {
        try {
            Statement statement = mainWindow.getDatabaseConnection().getConnection().createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT COUNT(user_accountid) AS 'count' FROM strongwhite_useraccounts WHERE user_accountid='"+accountIDTextField.getText().replace("\\","\\\\").replace("'","\\'")+"'");
            resultSet.next();
            if(!resultSet.getString("count").equals("0")&&!userCopy.getAccountID().equals(user.getAccountID())) {
                accountIDTextField.getErrorLabel().setText("Account ID already taked.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveUser() {
        user.setImage(accountIDTextField.getText()+".jpg");
        mainWindow.getDatabaseConnection().addUser(user, activeUser, userCopy);
    }
}
