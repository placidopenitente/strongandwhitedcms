package com.strongandwhite.pages.dentistadnstaff;

import com.strongandwhite.main.MainWindow;
import com.strongandwhite.models.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;

import java.io.IOException;

public class PreviewUser extends StackPane {
    @FXML private ImageView userImage;
    @FXML private HBox editUser;

    private User user;

    public PreviewUser(MainWindow mainWindow, UserAccounts userAccounts) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("PreviewUser.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        editUser.setOnMouseClicked(event-> {
            userAccounts.removeCover();
            mainWindow.gotoAddNewAccount(new AddNewAccount(user, mainWindow, mainWindow.getActiveUser()));
        });
    }

    public void setUser(User user) {
        this.user = user;
        userImage.setImage(this.user.getUserPhoto());
    }

    public User getUser() {
        return user;
    }
}
