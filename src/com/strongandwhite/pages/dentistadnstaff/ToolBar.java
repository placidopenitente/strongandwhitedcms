package com.strongandwhite.pages.dentistadnstaff;

import com.strongandwhite.main.MainWindow;
import com.strongandwhite.models.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.StackPane;

import java.io.IOException;

public class ToolBar extends StackPane {
    @FXML private StackPane addAccountButton;

    public ToolBar(MainWindow mainWindow, UserAccounts userAccounts) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("ToolBar.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        addAccountButton.setOnMouseClicked(event-> {
            userAccounts.removeCover();
            mainWindow.gotoAddNewAccount(new AddNewAccount(new User(), mainWindow, mainWindow.getActiveUser()));
        });
    }
}
