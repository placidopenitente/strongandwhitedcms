package com.strongandwhite.pages.dentistadnstaff;

import com.strongandwhite.main.MainWindow;
import com.strongandwhite.models.User;
import com.strongandwhite.pages.Page;
import com.strongandwhite.pages.PageTitle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import java.io.IOException;

public class UserAccounts extends Page {
    @FXML private TableView<User> accountsTable;
    @FXML private TableColumn<User, String> accountNumber;
    @FXML private TableColumn<User, String> accountID;
    @FXML private TableColumn<User, String> accountLastName;
    @FXML private TableColumn<User, String> accountFirstName;
    @FXML private TableColumn<User, String> accountMiddleName;
    @FXML private TableColumn<User, String> accountGender;
    @FXML private TableColumn<User, String> accountPosition;
    @FXML private TableColumn<User, String> accountType;
    @FXML private TableColumn<User, String> accountBirthdate;
    @FXML private TableColumn<User, String> accountContactNo;
    @FXML private TableColumn<User, String> accountAddress;
    @FXML private TableColumn<User, String> accountEmailAddress;
    @FXML private TableColumn<User, String> accountStatus;
    @FXML private TableColumn<User, String> accountNationality;
    @FXML private TableColumn<User, String> accountReligion;

    private ScrollPane contentPane;
    private MainWindow mainWindow;
    private Pane coverPane;
    private PreviewUser previewUser;
    private double x;
    private ToolBar toolBar;
    private AnchorPane toolBarPane;

    private ObservableList<User> users;

    public UserAccounts(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        users = FXCollections.observableArrayList();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("UserAccounts.fxml"));
        loader.setController(this);
        loader.setRoot(this);
        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        accountNumber.setCellValueFactory(new PropertyValueFactory<>("accountNumber"));
        accountID.setCellValueFactory(new PropertyValueFactory<>("accountID"));
        accountFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        accountMiddleName.setCellValueFactory(new PropertyValueFactory<>("middleName"));
        accountLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        accountAddress.setCellValueFactory(new PropertyValueFactory<>("address"));
        accountBirthdate.setCellValueFactory(new PropertyValueFactory<>("birthdate"));
        accountPosition.setCellValueFactory(new PropertyValueFactory<>("position"));
        accountType.setCellValueFactory(new PropertyValueFactory<>("accountType"));
        accountContactNo.setCellValueFactory(new PropertyValueFactory<>("contactNo"));
        accountGender.setCellValueFactory(new PropertyValueFactory<>("gender"));
        accountEmailAddress.setCellValueFactory(new PropertyValueFactory<>("emailAddress"));
        accountStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        accountNationality.setCellValueFactory(new PropertyValueFactory<>("nationality"));
        accountReligion.setCellValueFactory(new PropertyValueFactory<>("religion"));

        accountsTable.setItems(users);

        pageTitle = new PageTitle();
        pageTitle.setProgress(0);
        pageTitle.setTitle("User Accounts");
        pageTitle.setIcon(new Image("resources/images/User_36px.png"));

        mainWindow.getActivePagePane().getChildren().add(getPageTitle());
        coverPane = new Pane();
        coverPane.setPickOnBounds(false);
        mainWindow.getActivePagePane().getChildren().add(coverPane);
        previewUser = new PreviewUser(mainWindow, this);
        coverPane.getChildren().add(previewUser);

        previewUser.setVisible(false);

        accountsTable.getSelectionModel().selectedIndexProperty().addListener(event-> {
            if(accountsTable.getItems().size()>0) {
                previewUser.setVisible(true);
                previewUser.setLayoutY(((accountsTable.getSelectionModel().selectedIndexProperty().get()+3)*24)+pageTitle.getPrefHeight());
                previewUser.setUser(accountsTable.getSelectionModel().getSelectedItem());
            }
        });

        previewUser.setOnMousePressed(event -> x = event.getX());

        coverPane.setOnMouseDragged(event -> {
            previewUser.setLayoutX(event.getX()-x);
            if(previewUser.getLayoutX()<0) {
                previewUser.setLayoutX(0);
            }
            if(previewUser.getLayoutX()>(coverPane.getWidth()-previewUser.getWidth())) {
                previewUser.setLayoutX(coverPane.getWidth()-previewUser.getWidth());
            }
        });

        toolBar = new ToolBar(mainWindow, this);
        toolBarPane = new AnchorPane();
        toolBarPane.setPickOnBounds(false);
        toolBarPane.getChildren().add(toolBar);
        AnchorPane.setRightAnchor(toolBar, 25.0);
        AnchorPane.setBottomAnchor(toolBar, 25.0);
        mainWindow.getActivePagePane().getChildren().add(toolBarPane);

        mainWindow.getDatabaseConnection().loadAllUsers();
    }

    public User getSelectedUser() {
        return accountsTable.getSelectionModel().getSelectedItem();
    }

    public void selectUser(User user) {
        accountsTable.getSelectionModel().select(user);
    }

    public void setContentPane(ScrollPane contentPane) {
        this.contentPane = contentPane;
        accountsTable.prefHeightProperty().bind(contentPane.heightProperty().subtract(pageTitle.getPrefHeight()));
    }

    public ScrollPane getContentPane() {
        return contentPane;
    }

    public void setUsers(ObservableList<User> users) {
        this.users = users;
        accountsTable.setItems(users);
    }

    public ObservableList<User> getUsers() {
        return users;
    }

    public void removeCover() {
        mainWindow.getActivePagePane().getChildren().remove(coverPane);
        mainWindow.getActivePagePane().getChildren().remove(toolBarPane);
        mainWindow.getActivePagePane().getChildren().remove(pageTitle);
    }
}
