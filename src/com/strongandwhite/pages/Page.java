package com.strongandwhite.pages;

import com.strongandwhite.customcontrols.CustomControl;
import javafx.scene.layout.StackPane;

import java.util.ArrayList;

public class Page extends StackPane {
    protected ArrayList<CustomControl> customControls;
    protected PageTitle pageTitle;

    public void clearPageFields() {
        for (CustomControl customControl : customControls) {
            customControl.clear();
        }
    }

    public PageTitle getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(PageTitle pageTitle) {
        this.pageTitle = pageTitle;
    }

    public ArrayList<CustomControl> getCustomControls() {
        return customControls;
    }

    public void setCustomControls(ArrayList<CustomControl> customControls) {
        this.customControls = customControls;
    }
}
