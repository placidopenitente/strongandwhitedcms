package com.strongandwhite.pages;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class PageTitle extends VBox {
    @FXML private ProgressBar progressBar;
    @FXML private Label title;
    @FXML private ImageView icon;

    public PageTitle() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PageTitle.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public String getTitle() {
        return title.getText();
    }

    public void setProgress(double value) {
        progressBar.setProgress(value);
    }

    public double getProgress() {
        return progressBar.getProgress();
    }

    public void setIcon(Image image) {
        icon.setImage(image);
    }

    public Image getIcon() {
        return icon.getImage();
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }
}
