package com.strongandwhite.rules;

public class DateRule extends Rule {
    @Override
    public boolean validate(String text) {
        if(!text.matches("[\\d][\\d][\\d][\\d][-][\\d][\\d]?[-][\\d][\\d]?")) {
            setRuleViolation("Invalid Date Format.");
            return false;
        }
        return true;
    }
}
