package com.strongandwhite.rules;

public class NameRule extends Rule {

    private String fieldName;

    public NameRule(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public boolean validate(String text) {
        if(text.trim().equals("")) {
            setRuleViolation(""+fieldName+" is required.");
            return false;
        }
        return true;
    }
}
