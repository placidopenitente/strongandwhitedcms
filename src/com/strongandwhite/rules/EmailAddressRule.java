package com.strongandwhite.rules;

public class EmailAddressRule extends Rule {
    @Override
    public boolean validate(String text) {
        if(text.equals("")) {
            setRuleViolation("Email Address is required.");
            return false;
        }
        if(!text.matches("[\\w\\d]+([_-][\\w\\d]+)*@[\\w\\d]+([.][\\w\\d]+)+")) {
            setRuleViolation("Email Address Format is invalid.");
            return false;
        }
        return true;
    }
}
