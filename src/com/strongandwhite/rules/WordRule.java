package com.strongandwhite.rules;

public class WordRule extends Rule {
    private String fieldName;

    public WordRule(String fieldName) {
        this.fieldName = fieldName;
    }

    @Override
    public boolean validate(String text) {
        if(text.trim().equals("")) {
            setRuleViolation(""+fieldName+" is required.");
            return false;
        }
        return true;
    }
}
