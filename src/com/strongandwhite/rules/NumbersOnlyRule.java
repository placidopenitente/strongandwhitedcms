package com.strongandwhite.rules;

public class NumbersOnlyRule extends Rule {
    @Override
    public boolean validate(String text) {
        if(!text.matches("[\\d]+")) {
            setRuleViolation("Invalid Number Format.");
            return false;
        }
        return true;
    }
}
