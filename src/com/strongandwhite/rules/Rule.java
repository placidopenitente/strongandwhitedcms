package com.strongandwhite.rules;

import java.sql.Connection;

public abstract class Rule {
    private String ruleViolation;

    public abstract boolean validate(String text);

    public String getRuleViolation() {
        return ruleViolation;
    }

    public void setRuleViolation(String ruleViolation) {
        this.ruleViolation = ruleViolation;
    }
}
