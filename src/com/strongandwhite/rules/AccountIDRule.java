package com.strongandwhite.rules;

public class AccountIDRule extends Rule {

    @Override
    public boolean validate(String text) {
        if(text.equals("")) {
            setRuleViolation("Account ID is required.");
            return false;
        }
        if(!text.matches("[\\w\\d_]+")) {
            setRuleViolation("Invalid character found.");
            return false;
        }
        if(text.length()<8) {
            setRuleViolation("Account ID too short.");
            return false;
        }
        if(text.startsWith("_")||text.endsWith("_")) {
            setRuleViolation("Underscore on start and end is not allowed.");
            return false;
        }
        if(text.contains("__")) {
            setRuleViolation("Trailing underscores is not allowed.");
            return false;
        }
        return true;
    }
}
