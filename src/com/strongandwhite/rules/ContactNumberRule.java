package com.strongandwhite.rules;

public class ContactNumberRule extends Rule {
    @Override
    public boolean validate(String text) {
        if(text.equals("")) {
            setRuleViolation("Contact Number is required.");
            return false;
        }
        if(text.length()<2) {
            setRuleViolation("Contact Number is invalid.");
            return false;
        }
        if(!text.matches("[\\d]+")) {
            setRuleViolation("Only numbers are allowed.");
            return false;
        }
        return true;
    }
}
