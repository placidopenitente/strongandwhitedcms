package com.strongandwhite.rules;

public class AddressRule extends Rule {
    @Override
    public boolean validate(String text) {
        if(text.trim().equals("")) {
            setRuleViolation("Address is required.");
            return false;
        }
        return true;
    }
}
