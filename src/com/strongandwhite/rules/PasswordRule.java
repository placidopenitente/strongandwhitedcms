package com.strongandwhite.rules;

public class PasswordRule extends Rule {
    @Override
    public boolean validate(String text) {
        if(text.equals("")) {
            setRuleViolation("Password is required.");
            return false;
        }
        if(!text.matches("[\\w\\d]+")) {
            setRuleViolation("Invalid character found.");
            return false;
        }
        if(text.length()<8) {
            setRuleViolation("Password too short.");
            return false;
        }
        return true;
    }
}
